import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { NumbersList } from './NumbersList';
import { ListElement } from './ListElement/ListElement';


Enzyme.configure({ adapter: new Adapter() });

const bluePrint = {
    numbers: [0, 2, 4, 6, 10]
}

function prepareComponent(props) {
    return shallow(<NumbersList {...props}/>);
}

describe('<NumbersList/>', () => {
    let wrapper;

    it('should rendered without error', () => {
        wrapper = prepareComponent(bluePrint);
    });

    it('should render list of ListElement components', () => {
        wrapper = prepareComponent(bluePrint);
        const elements = wrapper.find(ListElement);

        expect(elements.length).toBe(bluePrint.numbers.length);
        elements.forEach((element, index) => {
            expect(element.prop('number') === bluePrint.numbers[index]).toBe(true);
        });
    });
});