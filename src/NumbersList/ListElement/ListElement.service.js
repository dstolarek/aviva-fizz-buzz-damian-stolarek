const WEDNESDAY = 3;

export default () => 
    parseInt(new Date().getDay()) === WEDNESDAY;
