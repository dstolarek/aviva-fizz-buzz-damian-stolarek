import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { ListElement } from './ListElement';
import ListElementService from './ListElement.service';


Enzyme.configure({ adapter: new Adapter() });

const bluePrint = {
    number: 2
}

function prepareComponent(props) {
    return shallow(<ListElement {...props}/>);
}

describe('<ListElement/>', () => {
    let wrapper;

    beforeEach(() => {
        Date.prototype.getDay = () => 1;
    });
  
    it('should rendered without error', () => {
        wrapper = prepareComponent(bluePrint);
    });

    it('should contain correct text', () => {
        wrapper = prepareComponent(bluePrint);

        expect(wrapper.text()).toEqual(bluePrint.number.toString());
    });

    describe('when today is not Wednesday', () => {
        beforeEach(() => {
            Date.prototype.getDay = () => 2;
        });

        describe('when number is divisible by 3', () => {
            it('then should render blue "fizz" text', () => {
                wrapper = prepareComponent({ ...bluePrint, number: 3 });
    
                expect(wrapper.text()).toEqual('fizz');
                expect(wrapper.find('p.blue').length).toBe(1);
            });
        });

        describe('when number is divisible by 5', () => {
            it('then should render green "buzz" text', () => {
                wrapper = prepareComponent({ ...bluePrint, number: 5 });
    
                expect(wrapper.text()).toEqual('buzz');
                expect(wrapper.find('p.green').length).toBe(1);
            });
        });
    });

    describe('when today is Wednesday', () => {
        beforeEach(() => {
            Date.prototype.getDay = () => 3;
        });

        describe('when number is divisible by 3', () => {
            it('then should render blue "wizz" text', () => {
                wrapper = prepareComponent({ ...bluePrint, number: 3 });

                expect(wrapper.text()).toEqual('wizz');
                expect(wrapper.find('p.blue').length).toBe(1);
            });
        });

        describe('when number is divisible by 5', () => {
            it('then should render green "wuzz" text', () => {
                wrapper = prepareComponent({ ...bluePrint, number: 5 });
    
                expect(wrapper.text()).toEqual('wuzz');
                expect(wrapper.find('p.green').length).toBe(1);
            });
        });
    });
    
});

