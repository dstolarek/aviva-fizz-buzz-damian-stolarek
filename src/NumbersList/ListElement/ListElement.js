import React from 'react';
import PropTypes from 'prop-types';
import './ListElement.css';

import checkIfIsWednesdayToday from './ListElement.service';

const getFizz = isAlrenative => <p className="Element blue">{isAlrenative ? 'wizz' : 'fizz'}</p>;
const getBuzz = isAlrenative => <p className="Element green">{isAlrenative ? 'wuzz' : 'buzz'}</p>;

function mapNumber(number) {
    const isDivisibleBy3 = number % 3 === 0;
    const isDivisibleBy5 = number % 5 === 0;

    if(isDivisibleBy3 && isDivisibleBy5) {
        return <p className="Element">{'fizz buzz'}</p>;
    } else if(isDivisibleBy3) {
        return getFizz(checkIfIsWednesdayToday());
    } else if(isDivisibleBy5) {
        return getBuzz(checkIfIsWednesdayToday());
    } else {
        return <p className="Element">{number}</p>;
    }
}

export const ListElement = ({ number }) => mapNumber(number);

ListElement.propTypes = {
    number: PropTypes.number.isRequired,
}