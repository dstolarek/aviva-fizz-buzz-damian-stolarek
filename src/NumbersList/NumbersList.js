import React from 'react';
import PropTypes from 'prop-types';

import { ListElement } from './ListElement/ListElement';
import './NumbersList.css';


function mapToListElement(number) {
    return <li key={number}><ListElement number={number}/></li>;
}

export const NumbersList = ({ numbers }) =>
    <ul className="List">{numbers.map(mapToListElement)}</ul>

NumbersList.propTypes = {
    numbers: PropTypes.arrayOf(PropTypes.number).isRequired
}