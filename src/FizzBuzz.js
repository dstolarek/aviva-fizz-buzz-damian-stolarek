import React, { Component } from 'react';

import { NumberField } from './NumberField/NumberField';
import { NumbersList } from './NumbersList/NumbersList';
import './FizzBuzz.css';


const MIN_NUMBER = 1;
const MAX_NUMBER = 1000;

class FizzBuzz extends Component {

  constructor(props) {
    super(props);

    this.state = {
      number: '',
      numbers: []
    };
  }

  render() {
    return (
      <div className="App">
         <NumberField
            value={this.state.number}
            onChange={this.updateNumbers}
            min={MIN_NUMBER}
            max={MAX_NUMBER}/>
          <NumbersList
            pagination={20}
            numbers={this.state.numbers}/>
      </div>
    );
  }

  updateNumbers = number => {
    const numbers = [];

    for(let i = 1; i < number; i++) {
      numbers.push(i);
    }

    this.setState({ numbers, number });
  }
}

export default FizzBuzz;
