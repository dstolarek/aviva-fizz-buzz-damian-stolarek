import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { NumberField } from './NumberField';


Enzyme.configure({ adapter: new Adapter() });

const bluePrint = {
    value: 21,
    onChange: jest.fn(),
    min: 1,
    max: 1000
}

function prepareComponent(props) {
    return shallow(<NumberField {...props}/>);
}

describe('<NumberField/>', () => {
    let wrapper;

    afterEach(() => {
        bluePrint.onChange.mockReset();
    });

    it('should rendered without error', () => {
        wrapper = prepareComponent(bluePrint);
    });
    
    it('should render input with correct attributes', () => {
        wrapper = prepareComponent(bluePrint);

        const input = wrapper.find('input');
        expect(input.prop('type')).toEqual('number');
        expect(input.prop('value')).toEqual(bluePrint.value);
        expect(input.prop('min')).toEqual(bluePrint.min);
        expect(input.prop('max')).toEqual(bluePrint.max);
    });

    describe('when user typed number into input', () => {
        describe('and when number is higer than min and less than max', () => {
            it('then "onChange" should be called with correct value', () => {
                const value = 20;
                wrapper = prepareComponent(bluePrint);

                const input = wrapper.find('input');
                expect(bluePrint.onChange.mock.calls.length).toBe(0);
                input.simulate('change', { target: { value } });
                expect(bluePrint.onChange.mock.calls.length).toBe(1);
                expect(bluePrint.onChange.mock.calls[0][0]).toBe(value);
            });
        });

        describe('and when number is out of scope', () => {
            it('then "onChange" should be called with old value', () => {
                const value1 = -1;
                const value2 = 11111;
                wrapper = prepareComponent(bluePrint);

                const input = wrapper.find('input');
                expect(bluePrint.onChange.mock.calls.length).toBe(0);
                input.simulate('change', { target: { value: value1 } });
                expect(bluePrint.onChange.mock.calls.length).toBe(1);
                expect(bluePrint.onChange.mock.calls[0][0]).toBe(bluePrint.value);
                input.simulate('change', { target: { value: value2 } });
                expect(bluePrint.onChange.mock.calls.length).toBe(2);
                expect(bluePrint.onChange.mock.calls[0][0]).toBe(bluePrint.value);
            });
        });
    });
});