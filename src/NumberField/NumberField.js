import React from 'react';
import PropTypes from 'prop-types';

import './NumberField.css';

export const NumberField = ({ value, onChange, min, max }) => {

    const changeHandler = ({ target }) => {
        const isValidNumber = /^[0-9]*$/.test(target.value);
        const canUpdateNumber = isValidNumber && min <= target.value && target.value <= max;

        if(canUpdateNumber) {
            onChange(target.value);
        } else {
            onChange(value);
        }
    }

    return <input placeholder="Enter a number" className="Input" type="number" value={value} onChange={changeHandler} min={min} max={max}/>;
}
    
NumberField.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    min: PropTypes.number,
    max: PropTypes.number,
};
